﻿namespace CS354 {
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Input;
    using Microsoft.Expression.Encoder.Devices;
    using Microsoft.Win32;
    using OpenCvSharp;
    using OpenCvSharp.Extensions;
    using Tesseract;
    using Point = OpenCvSharp.Point;
    using Rect = OpenCvSharp.Rect;
    using Size = OpenCvSharp.Size;
    using Window = System.Windows.Window;

    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        //Used for morphological operations
        static readonly InputArray CrossKernel = InputArray.Create(new byte[] {
            0, 1, 0,
            1, 1, 1,
            0, 1, 0
        });

        //For Matrix thread synchronization
        bool isBitmapConverting;

        //For webcam
        public Collection<EncoderDevice> VideoDevices { get; set; }

        public MainWindow() {
            this.InitializeComponent();
        }

        //Early Stage Controlled Code Activation Testing
        //void Window_KeyDown(object sender, KeyEventArgs e) {

        //    //var str = string.Empty;
        //    //try {
        //    //    using (var engine = new TesseractEngine(@"./tessdata", "eng", EngineMode.Default)) {
        //    //        //MessageBox.Show(engine.Version);
        //    //        using (var window = engine.Process(Cv2.ImRead("phototest.tif", ImreadModes.AnyColor).ToBitmap())) {
        //    //            str += window.GetText();
        //    //        }
        //    //    }
        //    //} catch (Exception ex) {
        //    //    MessageBox.Show($"Unexpected Error: {ex.Message}");
        //    //}
        //    //MessageBox.Show(str);

        //    //foreach (var uri in new[] { "BadInputLiveTest.jpg", "baseline.png", "dirty 2.png", "dirty.png", "Live Test.jpg", "test.png" }) {
        //    //    this.ParseTicTacToeImg(uri);
        //    //    MessageBox.Show("Finished!", "Next");
        //    //}
        //    //this.ParseTicTacToeImg("baselineFilled.png");
        //    this.ParseTicTacToeImg("BadInputLiveTest.jpg");
        //}

        /// <summary>
        ///     Overload for ParseTicTacToe.
        /// </summary>
        /// <param name="uri">Path to image</param>
        /// <param name="accuracyFiltersDebug"></param>
        /// <param name="houghTransformDebug"></param>
        /// <returns></returns>
        char[,] ParseTicTacToeImg(string uri, bool accuracyFiltersDebug = false, bool houghTransformDebug = false) {
            return this.ParseTicTacToeImg(Cv2.ImRead(uri, ImreadModes.AnyColor), accuracyFiltersDebug, houghTransformDebug);
        }

        /// <summary>
        ///     Main helper function for image processing. Method does simple image processing and adaptive thresholding
        ///     Before pushing the doctored binary image to the Hough Transformation callstack.
        ///     Also handles symbol recognition of segmented tic tac toe image.
        /// </summary>
        /// <param name="originalImg">Matrix image</param>
        /// <param name="accuracyFiltersDebug">Enables accuracy filter image checking</param>
        /// <param name="houghTransformDebug">Enables Hough Lines image checking</param>
        /// <returns></returns>
        char[,] ParseTicTacToeImg(Mat originalImg, bool accuracyFiltersDebug = false, bool houghTransformDebug = false) {
            //Start new thread and push image on to GUI thread for parallel update
            //Often the OpenCV C++ code will be so performative, it will execute
            //faster than the Managed code can push this code to a new thread
            //and operate the unmanaged bitmap conversion.
            this.isBitmapConverting = true;
            Task.Run(() => this.Dispatcher.Invoke(() => {
                this.Image.Source = originalImg.ToBitmapSource();
                this.isBitmapConverting = false;
                //Unmanaged Matrix dealloc
                originalImg.Dispose();
            }));

            var tttBoardImg = originalImg.Clone();

            var tttBoard = new Mat();

            try {
                Cv2.CvtColor(tttBoardImg, tttBoardImg, ColorConversionCodes.BGR2GRAY);
            } catch (OpenCVException ex) {
                MessageBox.Show(ex.Message, "Invalid Input Channel Color");
                return null;
            }


            //Blur image with mxn sampling window to reduce noise
            Cv2.GaussianBlur(tttBoardImg, tttBoardImg, new Size(7, 7), 0);

            if (accuracyFiltersDebug) {
                Cv2.ImShow("Gaussian Blur", tttBoardImg);
                Cv2.WaitKey(1);
            }

            //Reduce dependence on lighting, GaussianC seemed to remove too much detail
            Cv2.AdaptiveThreshold(tttBoardImg, tttBoard, 255, AdaptiveThresholdTypes.MeanC, ThresholdTypes.BinaryInv, 11, 7);

            if (accuracyFiltersDebug) {
                Cv2.ImShow("Adaptive Thresh", tttBoard);
                Cv2.WaitKey(1);
            }

            var ThresholdedImg = tttBoard.Clone();

            //Invert image intensity (Convert borders to 1's and background to 0's)
            //Cv2.BitwiseNot(tttBoard, tttBoard);

            //Try to connect broken lines due to binary inversion
            Cv2.Dilate(tttBoard, tttBoard, CrossKernel);

            if (accuracyFiltersDebug) {
                Cv2.ImShow("Board", tttBoard);
                Cv2.WaitKey(1);
            }

            //Restore original shape with connected lines
            var tttGrid = this.FilterTttGrid(ref tttBoard);
            Cv2.Erode(tttGrid, tttGrid, CrossKernel);

            if (accuracyFiltersDebug) {
                Cv2.ImShow("Input Filtered Tic Tac Toe Grid Space", tttGrid);
                Cv2.WaitKey(1);
            }

            //Generate Hough Accumulator and obtain rho, theta
            LineSegmentPolar[] houghSpace;
            using (var dilatedTttGrid = new Mat()) {
                Cv2.Dilate(tttGrid, dilatedTttGrid, CrossKernel);
                //rho_step = 1 pixel
                //theta_step = .1 degrees
                //minimum intersections = 200
                houghSpace = Cv2.HoughLines(tttGrid, 1, Cv2.PI / 180d / 10d, 200);
            }

            if (houghTransformDebug) {
                var tmp = tttGrid.Clone();
                foreach (var line in houghSpace) DrawLine(line, ref tmp);
                Cv2.ImShow("Raw Hough Lines", tmp);
                Cv2.WaitKey(1);
                tmp.Dispose();
            }

            MergeRelatedLines(houghSpace, ref tttBoard);

            if (houghTransformDebug) {
                var tmp = tttGrid.Clone();
                foreach (var line in houghSpace) DrawLine(line, ref tmp);
                Cv2.ImShow("Merged Hough Lines", tmp);
                Cv2.WaitKey(1);
                tmp.Dispose();
            }

            //Cv2.ImShow("Thresholded Integrity Check", ThresholdedImg);

            var cleanCpy = originalImg.Clone();
            Mat[,] playBoard = GetTttGrid(houghSpace, ref ThresholdedImg);
            var str = string.Empty;

            var ttt = new char[3, 3];

            if (playBoard != null)
                for (byte col = 0; col < 3; col++) {
                    for (byte row = 0; row < 3; row++) //Cv2.BitwiseNot(playBoard[col, row], playBoard[col, row]);
                        //Cv2.ImShow($"Block {col},{row}", playBoard[col, row]);

                        try {
                            using (var engine = new TesseractEngine(@"./tessdata", "eng", EngineMode.Default)) {
                                //MessageBox.Show(engine.Version);
                                engine.SetVariable("tessedit_char_whitelist", "XOxo");
                                engine.SetVariable("tessedit_char_blacklist", "ABCDEFGHIJLMNPQRSTUVWYZabcdefghijlmnpqrstuvwyz0123456789");
                                engine.DefaultPageSegMode = PageSegMode.SingleLine;
                                //Cv2.ImWrite($"{col}{row}.bmp", playBoard[col, row]);
                                using (var window = engine.Process(playBoard[row, col].ToBitmap())) {
                                    var symbol = (char) 0;
                                    if (window.GetText().ToUpper().Contains("X")) symbol = 'X';
                                    if (window.GetText().ToUpper().Contains("O")) symbol = 'O';

                                    //Cv2.ImShow($"Block {col},{row}, Detected = '{symbol}'", playBoard[row, col]);

                                    str += $"{symbol} ";
                                    ttt[row, col] = symbol;
                                }
                            }
                        } catch (Exception ex) {
                            MessageBox.Show($"Unexpected Error: {ex.Message}");
                        }
                    str += "\n";
                }


            //Notifies managed Garbage collector to context switch to unmanaged mode and call dealloc in unmanaged code
            cleanCpy.Dispose();
            tttGrid.Dispose();
            tttBoard.Dispose();
            tttBoardImg.Dispose();
            if (!this.isBitmapConverting) originalImg.Dispose();
            ThresholdedImg.Dispose();
            return ttt;
        }

        /// <summary>
        ///     Obtains the 4 intersection points from Hesse Normal Form defined lines.
        ///     This finds the middle box of the tic tac toe grid, which is assumed
        ///     to be the size of square for all boxes. A 3x3 dimensional array is
        ///     constructed using the extracted regions of interest.
        ///     It was found that attempting intersections with infinite slope
        ///     lines (vertical lines) caused issues, hence the need for Hough
        ///     Transform side calculations.
        /// </summary>
        /// <param name="lines">Hough Transformed line array</param>
        /// <param name="image"></param>
        /// <param name="Debug"></param>
        /// <returns></returns>
        static Mat[,] GetTttGrid(IEnumerable<LineSegmentPolar> lines, ref Mat image, bool Debug = false) {
            var topEdge = new Vec2f(float.PositiveInfinity, float.PositiveInfinity);
            var bottomEdge = new Vec2f(float.NegativeInfinity, float.NegativeInfinity);
            var leftEdge = new Vec2f(float.PositiveInfinity, float.PositiveInfinity);
            var rightEdge = new Vec2f(float.NegativeInfinity, float.NegativeInfinity);

            double topYIntercept = float.PositiveInfinity, topXIntercept = 0;
            double bottomYIntercept = 0, bottomXIntercept = 0;
            double leftXIntercept = float.PositiveInfinity, leftYIntercept = 0;
            double rightXIntercept = 0, rightYIntercept = 0;

            foreach (var current in lines) {
                var rho = current.Rho;
                var theta = current.Theta;

                if (float.IsNaN(rho) && float.IsNaN(theta)) continue;

                var xIntercept = rho / Math.Cos(theta);
                var yIntercept = rho / (Math.Cos(theta) * Math.Sin(theta));

                if (theta > Cv2.PI * 80 / 180 && theta < Cv2.PI * 100 / 180) {
                    if (rho < topEdge[0])

                        topEdge = new Vec2f(current.Rho, current.Theta);

                    if (rho > bottomEdge[0])
                        bottomEdge = new Vec2f(current.Rho, current.Theta);
                } else if (theta < Cv2.PI * 10 / 180 || theta > Cv2.PI * 170 / 180) {
                    if (xIntercept > rightXIntercept) {
                        rightEdge = new Vec2f(current.Rho, current.Theta);
                        rightXIntercept = xIntercept;
                    } else if (xIntercept <= leftXIntercept) {
                        leftEdge = new Vec2f(current.Rho, current.Theta);
                        leftXIntercept = xIntercept;
                    }
                }
            }

            if (float.IsInfinity(topEdge.Item0) || float.IsInfinity(topEdge.Item1)) {
                MessageBox.Show("TE");
                return null;
            }
            if (float.IsInfinity(bottomEdge.Item0) || float.IsInfinity(bottomEdge.Item1)) {
                MessageBox.Show("BE");
                return null;
            }
            if (float.IsInfinity(leftEdge.Item0) || float.IsInfinity(leftEdge.Item1)) {
                MessageBox.Show("LE");
                return null;
            }
            if (float.IsInfinity(rightEdge.Item0) || float.IsInfinity(rightEdge.Item1)) {
                MessageBox.Show("RE");
                return null;
            }

            //DrawLine(topEdge, ref image, 0, 0, 0);
            //DrawLine(bottomEdge, ref image, 0, 0, 0);
            //DrawLine(leftEdge, ref image, 0, 0, 0);
            //DrawLine(rightEdge, ref image, 0, 0, 0);

            if (Debug) Cv2.ImShow("Grid", image);

            var left1 = new Point2f();
            var left2 = new Point2f();
            var right1 = new Point2f();
            var right2 = new Point2f();
            var bottom1 = new Point2f();
            var bottom2 = new Point2f();
            var top1 = new Point2f();
            var top2 = new Point2f();

            var height = image.Height;

            var width = image.Width;

            if (leftEdge.Item1 >= 0) {
                left1.X = 0;
                left1.Y = (float) (leftEdge.Item0 / Math.Sin(leftEdge.Item1));
                left2.X = width;
                left2.Y = (float) (-left2.X / Math.Tan(leftEdge.Item1) + left1.Y);
            } else {
                left1.Y = 0;
                left1.X = (float) (leftEdge.Item0 / Math.Cos(leftEdge.Item1));
                left2.Y = height;
                left2.X = (float) (left1.X - height * Math.Tan(leftEdge.Item1));
            }

            if (rightEdge.Item1 >= 0) {
                right1.X = 0;
                right1.Y = (float) (rightEdge.Item0 / Math.Sin(rightEdge.Item1));
                right2.X = width;
                right2.Y = (float) (-right2.X / Math.Tan(rightEdge.Item1) + right1.Y);
            } else {
                right1.Y = 0;
                right1.X = (float) (rightEdge.Item0 / Math.Cos(rightEdge.Item1));
                right2.Y = height;
                right2.X = (float) (right1.X - height * Math.Tan(rightEdge.Item1));
            }

            bottom1.X = 0;
            bottom1.Y = (float) (bottomEdge.Item0 / Math.Sin(bottomEdge.Item1));

            bottom2.X = width;
            bottom2.Y = (float) (-bottom2.X / Math.Tan(bottomEdge.Item1) + bottom1.Y);

            top1.X = 0;
            top1.Y = (float) (topEdge.Item0 / Math.Sin(topEdge.Item1));
            top2.X = width;
            top2.Y = (float) (-top2.X / Math.Tan(topEdge.Item1) + top1.Y);

            // Next, we find the intersection of  these four lines
            double leftA = left2.Y - left1.Y;
            double leftB = left1.X - left2.X;

            var leftC = leftA * left1.X + leftB * left1.Y;

            double rightA = right2.Y - right1.Y;
            double rightB = right1.X - right2.X;

            var rightC = rightA * right1.X + rightB * right1.Y;

            double topA = top2.Y - top1.Y;
            double topB = top1.X - top2.X;

            var topC = topA * top1.X + topB * top1.Y;

            double bottomA = bottom2.Y - bottom1.Y;
            double bottomB = bottom1.X - bottom2.X;

            var bottomC = bottomA * bottom1.X + bottomB * bottom1.Y;

            // Intersection of left and top
            var detTopLeft = leftA * topB - leftB * topA;

            var ptTopLeft = new Point2f((float) ((topB * leftC - leftB * topC) / detTopLeft), (float) ((leftA * topC - topA * leftC) / detTopLeft));

            // Intersection of top and right
            var detTopRight = rightA * topB - rightB * topA;

            var ptTopRight = new Point2f((float) ((topB * rightC - rightB * topC) / detTopRight), (float) ((rightA * topC - topA * rightC) / detTopRight));

            // Intersection of right and bottom
            var detBottomRight = rightA * bottomB - rightB * bottomA;
            var ptBottomRight = new Point2f((float) ((bottomB * rightC - rightB * bottomC) / detBottomRight), (float) ((rightA * bottomC - bottomA * rightC) / detBottomRight));

            // Intersection of bottom and left
            var detBottomLeft = leftA * bottomB - leftB * bottomA;
            var ptBottomLeft = new Point2f((float) ((bottomB * leftC - leftB * bottomC) / detBottomLeft), (float) ((leftA * bottomC - bottomA * leftC) / detBottomLeft));

            double maxLength = (ptBottomLeft.X - ptBottomRight.X) * (ptBottomLeft.X - ptBottomRight.X) + (ptBottomLeft.Y - ptBottomRight.Y) * (ptBottomLeft.Y - ptBottomRight.Y);
            double temp = (ptTopRight.X - ptBottomRight.X) * (ptTopRight.X - ptBottomRight.X) + (ptTopRight.Y - ptBottomRight.Y) * (ptTopRight.Y - ptBottomRight.Y);

            if (temp > maxLength) maxLength = temp;

            temp = (ptTopRight.X - ptTopLeft.X) * (ptTopRight.X - ptTopLeft.X) + (ptTopRight.Y - ptTopLeft.Y) * (ptTopRight.Y - ptTopLeft.Y);

            if (temp > maxLength) maxLength = temp;

            temp = (ptBottomLeft.X - ptTopLeft.X) * (ptBottomLeft.X - ptTopLeft.X) + (ptBottomLeft.Y - ptTopLeft.Y) * (ptBottomLeft.Y - ptTopLeft.Y);

            if (temp > maxLength) maxLength = temp;

            maxLength = Math.Sqrt(maxLength);

            //Corner Mapping
            var src = new Point2f[4];

            //Top Left Box

            //TL
            src[0] = new Point2f((float) (ptTopLeft.X - maxLength), (float) (ptTopLeft.Y - maxLength));

            //TR
            src[1] = new Point2f(ptTopLeft.X, (float) (ptTopLeft.Y - maxLength));

            //BR
            src[2] = ptTopLeft;

            //BL
            src[3] = new Point2f((float) (ptTopLeft.X - maxLength), ptTopLeft.Y);

            if (src[0].X < 0) src[0].X = 0;
            if (src[0].Y < 0) src[0].Y = 0;

            if (src[1].X > image.Width) src[1].X = image.Width;
            if (src[1].Y < 0) src[1].Y = 0;

            if (src[2].X > image.Width) src[2].X = image.Width;
            if (src[2].Y > image.Width) src[2].Y = image.Width;

            if (src[3].X < 0) src[3].X = 0;
            if (src[3].Y > image.Width) src[3].Y = image.Width;

            var topLeft = new Mat(image, new Rect((Point) src[0], new Size(src[1].X - src[0].X, src[2].Y - src[1].Y)));

            var tmp = image.Clone();

            Cv2.Circle(tmp, (Point) src[0], 5, new Scalar(255, 0, 255, 0));
            Cv2.Circle(tmp, (Point) src[1], 5, new Scalar(255, 0, 255, 0));
            Cv2.Circle(tmp, (Point) src[2], 5, new Scalar(255, 0, 255, 0));
            Cv2.Circle(tmp, (Point) src[3], 5, new Scalar(255, 0, 255, 0));

            //Top Mid Box

            //TL
            src[0] = new Point2f(ptTopLeft.X, (float) (ptTopLeft.Y - maxLength));

            //TR
            src[1] = new Point2f(ptTopRight.X, (float) (ptTopRight.Y - maxLength));

            //BR
            src[2] = ptTopRight;

            //BL
            src[3] = ptTopLeft;

            if (src[0].X < 0) src[0].X = 0;
            if (src[0].Y < 0) src[0].Y = 0;

            if (src[1].X > image.Width) src[1].X = image.Width;
            if (src[1].Y < 0) src[1].Y = 0;

            if (src[2].X > image.Width) src[2].X = image.Width;
            if (src[2].Y > image.Width) src[2].Y = image.Width;

            if (src[3].X < 0) src[3].X = 0;
            if (src[3].Y > image.Width) src[3].Y = image.Width;

            var topMid = new Mat(image, new Rect((Point) src[0], new Size(src[1].X - src[0].X, src[2].Y - src[1].Y)));

            Cv2.Circle(tmp, (Point) src[0], 5, new Scalar(255, 0, 255, 0));
            Cv2.Circle(tmp, (Point) src[1], 5, new Scalar(255, 0, 255, 0));
            Cv2.Circle(tmp, (Point) src[2], 5, new Scalar(255, 0, 255, 0));
            Cv2.Circle(tmp, (Point) src[3], 5, new Scalar(255, 0, 255, 0));

            //Top Right Box

            //TL
            src[0] = new Point2f(ptTopRight.X, (float) (ptTopRight.Y - maxLength));

            //TR
            src[1] = new Point2f((float) (ptTopRight.X + maxLength), (float) (ptTopRight.Y - maxLength));

            //BR
            src[2] = new Point2f((float) (ptTopRight.X + maxLength), ptTopRight.Y);

            //BL
            src[3] = ptTopRight;

            if (src[0].X < 0) src[0].X = 0;
            if (src[0].Y < 0) src[0].Y = 0;

            if (src[1].X > image.Width) src[1].X = image.Width;
            if (src[1].Y < 0) src[1].Y = 0;

            if (src[2].X > image.Width) src[2].X = image.Width;
            if (src[2].Y > image.Width) src[2].Y = image.Width;

            if (src[3].X < 0) src[3].X = 0;
            if (src[3].Y > image.Width) src[3].Y = image.Width;

            var topRight = new Mat(image, new Rect((Point) src[0], new Size(src[1].X - src[0].X, src[2].Y - src[1].Y)));


            Cv2.Circle(tmp, (Point) src[0], 5, new Scalar(255, 0, 255, 0));
            Cv2.Circle(tmp, (Point) src[1], 5, new Scalar(255, 0, 255, 0));
            Cv2.Circle(tmp, (Point) src[2], 5, new Scalar(255, 0, 255, 0));
            Cv2.Circle(tmp, (Point) src[3], 5, new Scalar(255, 0, 255, 0));

            //Mid Left Box

            //TL
            src[0] = new Point2f((float) (ptTopLeft.X - maxLength), ptTopLeft.Y);

            //TR
            src[1] = ptTopLeft;

            //BR
            src[2] = ptBottomLeft;

            //BL
            src[3] = new Point2f((float) (ptBottomLeft.X - maxLength), ptBottomLeft.Y);

            if (src[0].X < 0) src[0].X = 0;
            if (src[0].Y < 0) src[0].Y = 0;

            if (src[1].X > image.Width) src[1].X = image.Width;
            if (src[1].Y < 0) src[1].Y = 0;

            if (src[2].X > image.Width) src[2].X = image.Width;
            if (src[2].Y > image.Width) src[2].Y = image.Width;

            if (src[3].X < 0) src[3].X = 0;
            if (src[3].Y > image.Width) src[3].Y = image.Width;

            var midLeft = new Mat(image, new Rect((Point) src[0], new Size(src[1].X - src[0].X, src[2].Y - src[1].Y)));


            Cv2.Circle(tmp, (Point) src[0], 5, new Scalar(255, 0, 255, 0));
            Cv2.Circle(tmp, (Point) src[1], 5, new Scalar(255, 0, 255, 0));
            Cv2.Circle(tmp, (Point) src[2], 5, new Scalar(255, 0, 255, 0));
            Cv2.Circle(tmp, (Point) src[3], 5, new Scalar(255, 0, 255, 0));

            //Center Box
            src[0] = ptTopLeft;
            src[1] = ptTopRight;
            src[2] = ptBottomRight;
            src[3] = ptBottomLeft;

            if (src[0].X < 0) src[0].X = 0;
            if (src[0].Y < 0) src[0].Y = 0;

            if (src[1].X > image.Width) src[1].X = image.Width;
            if (src[1].Y < 0) src[1].Y = 0;

            if (src[2].X > image.Width) src[2].X = image.Width;
            if (src[2].Y > image.Width) src[2].Y = image.Width;

            if (src[3].X < 0) src[3].X = 0;
            if (src[3].Y > image.Width) src[3].Y = image.Width;

            var midMid = new Mat(image, new Rect((Point) src[0], new Size(src[1].X - src[0].X, src[2].Y - src[1].Y)));
            if (Debug) Cv2.ImShow("midMid", midMid);

            //Mid Right Box

            //TL
            src[0] = ptTopRight;

            //TR
            src[1] = new Point2f((float) (ptTopRight.X + maxLength), ptTopRight.Y);

            //BR
            src[2] = new Point2f((float) (ptBottomRight.X + maxLength), ptBottomRight.Y);

            //BL
            src[3] = ptBottomRight;

            if (src[0].X < 0) src[0].X = 0;
            if (src[0].Y < 0) src[0].Y = 0;

            if (src[1].X > image.Width) src[1].X = image.Width;
            if (src[1].Y < 0) src[1].Y = 0;

            if (src[2].X > image.Width) src[2].X = image.Width;
            if (src[2].Y > image.Width) src[2].Y = image.Width;

            if (src[3].X < 0) src[3].X = 0;
            if (src[3].Y > image.Width) src[3].Y = image.Width;

            var midRight = new Mat(image, new Rect((Point) src[0], new Size(src[1].X - src[0].X, src[2].Y - src[1].Y)));

            Cv2.Circle(tmp, (Point) src[0], 5, new Scalar(255, 0, 255, 0));
            Cv2.Circle(tmp, (Point) src[1], 5, new Scalar(255, 0, 255, 0));
            Cv2.Circle(tmp, (Point) src[2], 5, new Scalar(255, 0, 255, 0));
            Cv2.Circle(tmp, (Point) src[3], 5, new Scalar(255, 0, 255, 0));

            //Bottom Left Box

            //TL
            src[0] = new Point2f((float) (ptBottomLeft.X - maxLength), ptBottomLeft.Y);

            //TR
            src[1] = ptBottomLeft;

            //BR
            src[2] = new Point2f(ptBottomLeft.X, (float) (ptBottomLeft.Y + maxLength));

            //BL
            src[3] = new Point2f((float) (ptBottomLeft.X - maxLength), (float) (ptBottomLeft.Y + maxLength));

            if (src[0].X < 0) src[0].X = 0;
            if (src[0].Y < 0) src[0].Y = 0;

            if (src[1].X > image.Width) src[1].X = image.Width;
            if (src[1].Y < 0) src[1].Y = 0;

            if (src[2].X > image.Width) src[2].X = image.Width;
            if (src[2].Y > image.Width) src[2].Y = image.Width;

            if (src[3].X < 0) src[3].X = 0;
            if (src[3].Y > image.Width) src[3].Y = image.Width;

            var bottomLeft = new Mat(image, new Rect((Point) src[0], new Size(src[1].X - src[0].X, src[2].Y - src[1].Y)));


            Cv2.Circle(tmp, (Point) src[0], 5, new Scalar(255, 0, 255, 0));
            Cv2.Circle(tmp, (Point) src[1], 5, new Scalar(255, 0, 255, 0));
            Cv2.Circle(tmp, (Point) src[2], 5, new Scalar(255, 0, 255, 0));
            Cv2.Circle(tmp, (Point) src[3], 5, new Scalar(255, 0, 255, 0));

            //Bottom Mid Box

            //TL
            src[0] = ptBottomLeft;

            //TR
            src[1] = ptBottomRight;

            //BR
            src[2] = new Point2f(ptBottomRight.X, (float) (ptBottomRight.Y + maxLength));

            //BL
            src[3] = new Point2f(ptBottomLeft.X, (float) (ptBottomLeft.Y + maxLength));

            if (src[0].X < 0) src[0].X = 0;
            if (src[0].Y < 0) src[0].Y = 0;

            if (src[1].X > image.Width) src[1].X = image.Width;
            if (src[1].Y < 0) src[1].Y = 0;

            if (src[2].X > image.Width) src[2].X = image.Width;
            if (src[2].Y > image.Width) src[2].Y = image.Width;

            if (src[3].X < 0) src[3].X = 0;
            if (src[3].Y > image.Width) src[3].Y = image.Width;

            var bottomMid = new Mat(image, new Rect((Point) src[0], new Size(src[1].X - src[0].X, src[2].Y - src[1].Y)));

            Cv2.Circle(tmp, (Point) src[0], 5, new Scalar(255, 0, 255, 0));
            Cv2.Circle(tmp, (Point) src[1], 5, new Scalar(255, 0, 255, 0));
            Cv2.Circle(tmp, (Point) src[2], 5, new Scalar(255, 0, 255, 0));
            Cv2.Circle(tmp, (Point) src[3], 5, new Scalar(255, 0, 255, 0));

            //Bottom Right Box

            //TL
            src[0] = ptBottomRight;

            //TR
            src[1] = new Point2f((float) (ptBottomRight.X + maxLength), ptBottomRight.Y);

            //BR
            src[2] = new Point2f((float) (ptBottomRight.X + maxLength), (float) (ptBottomRight.Y + maxLength));

            //BL
            src[3] = new Point2f(ptBottomRight.X, (float) (ptBottomRight.Y + maxLength));

            if (src[0].X < 0) src[0].X = 0;
            if (src[0].Y < 0) src[0].Y = 0;

            if (src[1].X > image.Width) src[1].X = image.Width;
            if (src[1].Y < 0) src[1].Y = 0;

            if (src[2].X > image.Width) src[2].X = image.Width;
            if (src[2].Y > image.Width) src[2].Y = image.Width;

            if (src[3].X < 0) src[3].X = 0;
            if (src[3].Y > image.Width) src[3].Y = image.Width;

            var bottomRight = new Mat(image, new Rect((Point) src[0], new Size(src[1].X - src[0].X, src[2].Y - src[1].Y)));

            Cv2.Circle(tmp, (Point) src[0], 5, new Scalar(255, 0, 255, 0));
            Cv2.Circle(tmp, (Point) src[1], 5, new Scalar(255, 0, 255, 0));
            Cv2.Circle(tmp, (Point) src[2], 5, new Scalar(255, 0, 255, 0));
            Cv2.Circle(tmp, (Point) src[3], 5, new Scalar(255, 0, 255, 0));

            var tttGrid = new Mat[3, 3];

            tttGrid[0, 0] = topLeft;
            tttGrid[1, 0] = topMid;
            tttGrid[2, 0] = topRight;

            tttGrid[0, 1] = midLeft;
            tttGrid[1, 1] = midMid;
            tttGrid[2, 1] = midRight;

            tttGrid[0, 2] = bottomLeft;
            tttGrid[1, 2] = bottomMid;
            tttGrid[2, 2] = bottomRight;


            //Cv2.ImShow("Regions of Interest", tmp);
            tmp.Dispose();

            return tttGrid;
        }

        /// <summary>
        ///     Draws Hough Lines on image
        /// </summary>
        /// <param name="line">Line in Hesse normal form</param>
        /// <param name="img">Pointer to image (readonly)</param>
        static void DrawLine(LineSegmentPolar line, ref Mat img, byte r = 255, byte g = 255, byte b = 255) {
            var rgb = new Scalar(r, g, b);
            if (line.Theta >= 0) {
                //Figured this out from the Hough slides
                //Converts Polar Coordinates back to normal form
                var m = -1 / Math.Tan(line.Theta);
                var c = line.Rho / Math.Sin(line.Theta);

                Cv2.Line(img, new Point(0, c), new Point(img.Width, m * img.Width + c), rgb);
            } else {
                Cv2.Line(img, new Point(line.Rho, 0), new Point(line.Rho, img.Height), rgb);
            }
        }

        /// <summary>
        ///     Helper for Vector defined hesse coordinates
        /// </summary>
        /// <param name="line"></param>
        /// <param name="img"></param>
        /// <param name="r"></param>
        /// <param name="g"></param>
        /// <param name="b"></param>
        static void DrawLine(Vec2f line, ref Mat img, byte r = 255, byte g = 255, byte b = 255) {
            DrawLine(new LineSegmentPolar(line.Item0, line.Item1), ref img, r, g, b);
        }

        /// <summary>
        ///     Obtains the largest pixel connected object in binary image, which is assumed
        ///     to be the tic tac toe grid and returns only, this object.
        /// </summary>
        /// <param name="image">reference (pointer) to binarized input image. Data is passed by ref for speed</param>
        /// <param name="binImage">Binary thresholded image</param>
        /// <returns>Input filtered (Ideally) Tic Tac Toe Grid</returns>
        Mat FilterTttGrid(ref Mat binImage) {
            var bestArea = -1;
            var bestSeedPt = new Point(-1, -1);

            //Iterator, as I cannot manually traverse pointers from managed code
            var matI = new MatOfByte(binImage);
            MatIndexer<byte> indexer = matI.GetIndexer();

            //Find largest connected object, mark smaller objects gray
            for (var y = 0; y < binImage.Height; y++) {
                for (var x = 0; x < binImage.Width; x++) {
                    var intensity = indexer[y, x];
                    if (intensity < 128) continue;
                    var area = Cv2.FloodFill(binImage, new Point(x, y), new Scalar(64));

                    if (area <= bestArea) continue;
                    bestArea = area;
                    bestSeedPt = new Point(x, y);
                }
            }

            if (bestSeedPt.X < 0 || bestSeedPt.Y < 0 || bestArea == -1) throw new ArgumentException("Empty Image");

            //Mark the largest connected object at full intensity from seed point
            var filtered = binImage.Clone();
            Cv2.FloodFill(filtered, bestSeedPt, new Scalar(255));

            //Iterator, as I cannot manually traverse pointers from managed code
            matI = new MatOfByte(filtered);
            indexer = matI.GetIndexer();

            //Remove greyed continuous binary objects
            for (var y = 0; y < filtered.Height; y++) {
                for (var x = 0; x < filtered.Width; x++)
                    if (indexer[y, x] == 64 && x != bestSeedPt.X && y != bestSeedPt.Y)
                        Cv2.FloodFill(filtered, new Point(x, y), new Scalar(0));
            }

            //Dispose interfaces unmanaged dealloc calls to managed mechanisms
            matI.Dispose();
            return filtered;
        }

        /// <summary>
        ///     Merges similar Hough Lines using the mean function based on input tolerances.
        ///     All calculations done using the hesse normal form of line:
        ///     http://www.aishack.in/tutorials/converting-lines-normal-slopeintercept-form/
        /// </summary>
        /// <param name="lines">Hough Line Array</param>
        /// <param name="img">Pointer to image (not modified)</param>
        /// <param name="rhoTolerance">rho space tolerance</param>
        /// <param name="thetaTolerance">theta space tolerance</param>
        /// <param name="endPointTolerance">boundary points tolerance</param>
        static void MergeRelatedLines(LineSegmentPolar[] lines, ref Mat img, float rhoTolerance = 40f, float thetaTolerance = 67.5f, uint endPointTolerance = 128) {
            for (uint i = 0; i < lines.Length; i++) {
                //Hough Line already considered
                if (float.IsNaN(lines[i].Rho) && float.IsNaN(lines[i].Theta)) continue;

                var p1 = lines[i].Rho;
                var theta1 = lines[i].Theta;
                var pt1Current = new Point(0, 0);
                var pt2Current = new Point(0, 0);

                if (theta1 > Cv2.PI * 45 / 180f && theta1 < Cv2.PI * (180f - 45) / 180f) {
                    //Establish line bounds
                    pt1Current.X = 0;
                    pt1Current.Y = (int) Math.Round(p1 / Math.Sin(theta1));

                    pt2Current.X = img.Width;
                    pt2Current.Y = (int) Math.Round(-pt2Current.X / Math.Tan(theta1) + p1 / Math.Sin(theta1));
                } else {
                    //Establish line bounds
                    pt1Current.Y = 0;
                    pt1Current.X = (int) Math.Round(p1 / Math.Cos(theta1));

                    pt2Current.Y = img.Height;
                    pt2Current.X = (int) Math.Round(-pt2Current.Y / Math.Tan(theta1) + p1 / Math.Cos(theta1));
                }

                for (uint j = 0; j < lines.Length; j++) {
                    if (i == j) continue;
                    if (!(Math.Abs(lines[j].Rho - lines[i].Rho) < rhoTolerance) || !(Math.Abs(lines[j].Theta - lines[i].Theta) < Cv2.PI * thetaTolerance / 180f)) continue;
                    var p = lines[j].Rho;
                    var theta = lines[j].Theta;
                    Point pt1, pt2;
                    if (lines[j].Theta > Cv2.PI * 45 / 180f && lines[j].Theta < Cv2.PI * (180 - 45) / 180f) {
                        //Establish line bounds
                        pt1.X = 0;
                        pt1.Y = (int) Math.Round(p / Math.Sin(theta));

                        pt2.X = img.Width;
                        pt2.Y = (int) Math.Round(-pt2.X / Math.Tan(theta) + p / Math.Sin(theta));
                    } else {
                        //Establish line bounds
                        pt1.Y = 0;
                        pt1.X = (int) Math.Round(p / Math.Cos(theta));

                        pt2.Y = img.Height;
                        pt2.X = (int) Math.Round(-pt2.Y / Math.Tan(theta) + p / Math.Cos(theta));
                    }

                    //If endpoints are close merge the lines
                    if (!((double) (pt1.X - pt1Current.X) * (pt1.X - pt1Current.X) + (pt1.Y - pt1Current.Y) * (pt1.Y - pt1Current.Y) < endPointTolerance * endPointTolerance)
                        || !((double) (pt2.X - pt2Current.X) * (pt2.X - pt2Current.X) + (pt2.Y - pt2Current.Y) * (pt2.Y - pt2Current.Y) < endPointTolerance * endPointTolerance)) continue;

                    // Merge the two
                    lines[i].Rho = (lines[i].Rho + lines[j].Rho) / 2f;
                    lines[i].Theta = (lines[i].Theta + lines[j].Theta) / 2f;

                    //Marked invalid to decrease repetition
                    lines[j].Rho = float.NaN;
                    lines[j].Theta = float.NaN;
                }
            }
        }

        void FileButton_PreviewMouseDown(object sender, MouseButtonEventArgs e) {
            var openFileDlg = new OpenFileDialog {
                DefaultExt = ".png",
                Filter = "Image Files | *.png;*.bmp;*.dib;*.jpeg;*.jpg;*.jpe;*.jp2;*.webp;*.tiff;*.tif"
            };
            if (openFileDlg.ShowDialog() != true) return;
            char[,] ttt = this.ParseTicTacToeImg(openFileDlg.FileName);

            if (ttt == null) {
                MessageBox.Show("No Grid Detected.");
                return;
            }

            this.TL.Content = ttt[0, 0];
            this.TM.Content = ttt[1, 0];
            this.TR.Content = ttt[2, 0];

            this.ML.Content = ttt[0, 1];
            this.MM.Content = ttt[1, 1];
            this.MR.Content = ttt[2, 1];

            this.BL.Content = ttt[0, 2];
            this.BM.Content = ttt[1, 2];
            this.BR.Content = ttt[2, 2];
        }

        void PhotoButton_PreviewMouseDown(object sender, MouseButtonEventArgs e) {
            MessageBox.Show("Take Photo Feature is extremely buggy, I would recommend taking pictures with the built in Windows Camera App.\n\nThis is here as proof of concept.");

            var cap = new VideoCapture(0);
            // open the default camera, use something different from 0 otherwise;
            // Check VideoCapture documentation.
            cap.Open(0);
            if (!cap.IsOpened()) {
                MessageBox.Show("No Cameras Detected");
                return;
            }

            //Camera Priming
            for (var i = 0; i < 30; i++) {
                cap.Grab();
            }
            
            var img = new Mat();

            while (img.Empty()) {
                cap.Read(img);
            }

            //equivalent to unmanaged c++ Cap.close();
            cap.Dispose();

            char[,] ttt = this.ParseTicTacToeImg(img);

            if (ttt == null)
            {
                MessageBox.Show("No Grid Detected.");
                return;
            }

            this.TL.Content = ttt[0, 0];
            this.TM.Content = ttt[1, 0];
            this.TR.Content = ttt[2, 0];

            this.ML.Content = ttt[0, 1];
            this.MM.Content = ttt[1, 1];
            this.MR.Content = ttt[2, 1];

            this.BL.Content = ttt[0, 2];
            this.BM.Content = ttt[1, 2];
            this.BR.Content = ttt[2, 2];
        }

        //    //y = (-cotθ)*x + (p*cosecθ)
        //Point2f Intersection(LineSegmentPolar line1, LineSegmentPolar line2, ref Mat tttGrid) {
        ///// <returns></returns>

        //Point2f Intersection(Vec2f line1, Vec2f line2, ref Mat tttGrid) {
        //    return this.Intersection(new LineSegmentPolar(line1.Item0, line2.Item1), new LineSegmentPolar(line2.Item0, line2.Item1), ref tttGrid);
        //}

        ///// <summary>
        ///// Converts Polar Coordinates to Normal Form and finds intersection.
        ///// </summary>
        ///// <param name="line1">First reference Line</param>
        ///// <param name="line2">Second reference Line</param>

        ///// <param name="tttGrid">(Readonly) Pointer to Image</param>
        //    float y;

        //    //cot -> Math.PI/2 - Math.Atan(x)
        //    //Courtesy Daniel Martin: https://stackoverflow.com/questions/2779230/inverse-cot-in-c-sharp
        //    //csc -> 1/Math.Sin(x)

        //    MessageBox.Show()
        //    var line1Pt_I = new Point2f(0, (float)((-Math.PI / 2 - Math.Atan(line1.Theta)) * 0 + (line1.Rho * (1 / Math.Sin(line1.Theta)))));
        //    var line1Pt_F = new Point2f(tttGrid.Width, (float)((-Math.PI / 2 - Math.Atan(line1.Theta)) * tttGrid.Width + (line1.Rho * (1 / Math.Sin(line1.Theta)))));

        //    var line2Pt_I = new Point2f(0, (float)((-Math.PI / 2 - Math.Atan(line2.Theta)) * 0 + (line2.Rho * (1 / Math.Sin(line2.Theta)))));
        //    var line2Pt_F = new Point2f(tttGrid.Width, (float)((-Math.PI / 2 - Math.Atan(line2.Theta)) * tttGrid.Width + (line2.Rho * (1 / Math.Sin(line2.Theta)))));

        //    return Intersection(line1Pt_I, line1Pt_F, line2Pt_I, line2Pt_F);
        //}

        /// <summary>
        ///     OpenCV does not contain a definiton for line intersection.
        ///     Courtesy Andrey Kamaev via StackOverflow:
        ///     https://stackoverflow.com/questions/7446126/opencv-2d-line-intersection-helper-function/7448287#7448287
        ///     Finds the intersection of two lines, or returns false.
        ///     The lines are defined by (o1, p1) and (o2, p2).
        /// </summary>
        /// <param name="o1"></param>
        /// <param name="p1"></param>
        /// <param name="o2"></param>
        /// <param name="p2"></param>
        /// <returns></returns>
        //static Point2f Intersection(Point2f o1, Point2f p1, Point2f o2, Point2f p2) {
        //    var x = o2 - o1;
        //    var d1 = p1 - o1;
        //    var d2 = p2 - o2;

        //    var cross = d1.X * d2.Y - d1.Y * d2.X;
        //    //if (Math.Abs(cross) < /*EPS*/1e-8) return new Point2f(float.NaN, float.NaN);

        //    double t1 = (x.X * d2.Y - x.Y * d2.X) / cross;
        //    return o1 + d1 * t1;
        //}

        //    //Cv2.Threshold(img, img, 127, 255, ThresholdTypes.Binary);

        /// <summary>
        ///     It seems a lot of image processing kits do not have skeletonization
        ///     This is a C# port of this C implementation:
        ///     http://felix.abecassis.me/2011/09/opencv-morphological-skeleton/
        /// </summary>
        /// <param name="img"></param>

        //void MorphologicalSkeletonize(ref Mat img) {
        //    using (var skel = new Mat(img.Size(), MatType.CV_8UC1, new Scalar(0)))
        //    using (var eroded = new Mat())
        //    using (var temp = new Mat()) {
        //        bool done;
        //        do {
        //            Cv2.Erode(img, eroded, CrossKernel);
        //            Cv2.Dilate(eroded, temp, CrossKernel); // temp = open(img)
        //            Cv2.Subtract(img, temp, temp);
        //            Cv2.BitwiseOr(skel, temp, skel);

        //            Cv2.ImShow("Skeletonizing: ", eroded);
        //            Cv2.WaitKey(100);

        //            eroded.CopyTo(img);

        //            done = (Cv2.CountNonZero(img) == 0);
        //        } while (!done);
        //    }
        //}
    }
}